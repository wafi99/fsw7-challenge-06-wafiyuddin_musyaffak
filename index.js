const express = require("express")
const swaggerUI = require("swagger-ui-express")
const jwt = require("jsonwebtoken")
const app = express()
const {
    PORT = 8080
} = process.env

app.use(express.json())


// https://www.npmjs.com/package/swagger-ui-express
const options = {
    swaggerOptions: {
        url: "/api-docs"
    }
}
app.use("/docs", swaggerUI.serve, swaggerUI.setup(null, options))
app.get("/api-docs", (req, res) => {
    res.sendFile(__dirname + "/swagger.yaml")
})

const authenticated = (req, res, next) => {
    try {
        let header = req.headers.authorization.split("Bearer ")[1]
        console.log(header)
        let user = jwt.verify(header, 's3cr3t')
        if (user.username) {
            // user.role
            // user.address
            // user.id
            req.user = user
            next()
            return
        }
    } catch (err) {
        console.log(err.message)
    }

    res.status(401).json({
        message: "invalid token"
    })
}

app.get("/api/v1/profile", authenticated, (req, res) => {
    res.status(200).json(req.user)
})

// LOGIN DAN REGISTER START
app.post("/api/v1/login", (req, res) => {
    if (req.body.username === "sabrina@binaracademy.com" &&
        req.body.password === "password") {
        let user = {
            id: 1,
            username: req.body.username,
            role: "SUPERADMIN",
            fullname: "Sabrina",
            phone_number: "+62813238427398",
            address: "jakarta",
        }

        let token = jwt.sign(user, 's3cr3t')

        res.status(200).json({
            token: token
        })
        return
    }

    res.status(401).json({
        message: "invalid email or password"
    })
})

app.post("/api/v1/register", (req, res) => {
    if (req.body.username === "sabrina@binaracademy.com" &&
        req.body.password === "password" &&
        req.body.age === 24 &&
        req.body.phone_number === +62813238427398 &&
        req.body.address === "Jakarta") {
        let user = {
            id: 1,
            username: req.body.username,
            role: "SUPERADMIN",
            fullname: "Sabrina",
            phone_number: "+62813238427398",
            address: "Jakarta",
        }

        let token = jwt.sign(user, 's3cr3t')

        res.status(200).json({
            token: token
        })
        return
    }

    res.status(401).json({
        message: "invalid email or password"
    })
})
// LOGIN DAN REGISTER END

// ADMINS START
app.post("/api/v1/admins", (req, res) => {
    if (req.body.username === "sabrina@binaracademy.com" &&
        req.body.password === "password") {
        let user = {
            id: 1,
            username: req.body.username,
            role: "SUPERADMIN",
            fullname: "Sabrina",
            phone_number: "+62813238427398",
            address: "jakarta",
        }
        let token = jwt.sign(user, 's3cr3t')

        res.status(200).json({
            token: token
        })
        return
    }

    res.status(401).json({
        message: "invalid SUPERADMIN"
    })
})
app.get("/api/v1/admins", authenticated, (req, res) => {
    res.status(200).json(req.user)
})
// ADMINS END

// ADMIN ID START
app.put("/api/v1/admins/1", authenticated, (req, res) => {
    res.status(200).json(req.user)
})
app.get("/api/v1/admins/1", authenticated, (req, res) => {
    res.status(200).json(req.user)
})
app.delete("/api/v1/admins/1", authenticated, (req, res) => {
    res.status(200).json(req.user)
})
// ADMIN ID END

// CARS START
app.post("/api/v1/cars", (req, res) => {
    if (req.body.id === 1 &&
        req.body.name === "Avanza" &&
        req.body.price === 100000.2 &&
        req.body.photo === "https://google.com/image.png" &&
        req.body_created_at === "2017-07-21T17:32:28Z") {
        let user = {
            id: 1,
            username: req.body.username,
            role: "SUPERADMIN",
            fullname: "Sabrina",
            phone_number: "+62813238427398",
            address: "jakarta",
        }
        let token = jwt.sign(user, 's3cr3t')

        res.status(200).json({
            token: token
        })
        return
    }

    res.status(401).json({
        message: "invalid Cars"
    })
})
app.get("/api/v1/admins", (req, res) => {
    res.status(200).json(req.user)
})
// CARS END

// CARS ID START
app.put("/api/v1/cars/1", authenticated, (req, res) => {
    res.status(200).json(req.user)
})
app.get("/api/v1/cars/1", (req, res) => {
    res.status(200).json(req.user)
})
app.delete("/api/v1/cars/1", authenticated, (req, res) => {
    res.status(200).json(req.user)
})
// CARS ID END
app.listen(PORT, () => {
    console.log(`running on port ${PORT}`)
})